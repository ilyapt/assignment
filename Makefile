CONFIGGEN = configtxgen -configPath ./configs
IMAGE_TAG = 2.0.1
COMPOSE = COMPOSE_PROJECT_NAME=sample IMAGE_TAG=${IMAGE_TAG} docker-compose
ORDERER1 = --tls true -o orderer1.example.com:7050 --cafile ./crypto-config/ordererOrganizations/example.com/orderers/orderer1.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
ORG1 = -e CORE_PEER_LOCALMSPID=Org1MSP -e CORE_PEER_ADDRESS=peer0.org1.example.com:7051 -e CORE_PEER_MSPCONFIGPATH=crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp -e CORE_PEER_TLS_ROOTCERT_FILE=crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
ORG2 = -e CORE_PEER_LOCALMSPID=Org2MSP -e CORE_PEER_ADDRESS=peer0.org2.example.com:8051 -e CORE_PEER_MSPCONFIGPATH=crypto-config/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp -e CORE_PEER_TLS_ROOTCERT_FILE=crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
ORG3 = -e CORE_PEER_LOCALMSPID=Org3MSP -e CORE_PEER_ADDRESS=peer0.org3.example.com:9051 -e CORE_PEER_MSPCONFIGPATH=crypto-config/peerOrganizations/org3.example.com/users/Admin@org3.example.com/msp -e CORE_PEER_TLS_ROOTCERT_FILE=crypto-config/peerOrganizations/org3.example.com/peers/peer0.org3.example.com/tls/ca.crt
TWOPEERS = --peerAddresses peer0.org1.example.com:7051 --tlsRootCertFiles crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses peer0.org2.example.com:8051 --tlsRootCertFiles crypto-config/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
OWNER = -e CORE_PEER_MSPCONFIGPATH=crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp
USER = -e CORE_PEER_MSPCONFIGPATH=crypto-config/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp


crypto:
	cryptogen generate --config=configs/crypto-config.yaml
	openssl x509 -in crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/signcerts/User1@org1.example.com-cert.pem \
		-pubkey -noout | openssl ec -pubin -outform d | dd ibs=26 skip=1 | od -A n -v -t x1 | tr -d ' \n' > owner.pub
	openssl x509 -in crypto-config/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp/signcerts/User1@org2.example.com-cert.pem \
		-pubkey -noout | openssl ec -pubin -outform d | dd ibs=26 skip=1 | od -A n -v -t x1 | tr -d ' \n' > user.pub
artifacts:
	[ -d artifacts ] || mkdir artifacts
	${CONFIGGEN} -profile Genesis -outputBlock ./artifacts/genesis.block -channelID systemchannel
	${CONFIGGEN} -profile Channel -channelID mychannel -outputCreateChannelTx ./artifacts/mychannel.tx
	${CONFIGGEN} -profile Channel -outputAnchorPeersUpdate ./artifacts/Org1MSPanchors.tx -channelID mychannel -asOrg Org1MSP
	${CONFIGGEN} -profile Channel -outputAnchorPeersUpdate ./artifacts/Org2MSPanchors.tx -channelID mychannel -asOrg Org2MSP
	${CONFIGGEN} -profile Channel -outputAnchorPeersUpdate ./artifacts/Org3MSPanchors.tx -channelID mychannel -asOrg Org3MSP
up:
	${COMPOSE} -f configs/docker-compose.yaml up -d
down:
	${COMPOSE} -f configs/docker-compose.yaml down --volumes --remove-orphans
channel:
	docker exec $(ORG1) cli peer channel create -c mychannel $(ORDERER1) -f ./artifacts/mychannel.tx --outputBlock ./artifacts/mychannel.block
	docker exec $(ORG1) cli peer channel join --tls true -b ./artifacts/mychannel.block
	docker exec $(ORG2) cli peer channel join --tls true -b ./artifacts/mychannel.block
	docker exec $(ORG3) cli peer channel join --tls true -b ./artifacts/mychannel.block
	docker exec $(ORG1) cli peer channel update -c mychannel $(ORDERER1) -f ./artifacts/Org1MSPanchors.tx
	docker exec $(ORG2) cli peer channel update -c mychannel $(ORDERER1) -f ./artifacts/Org2MSPanchors.tx
	docker exec $(ORG3) cli peer channel update -c mychannel $(ORDERER1) -f ./artifacts/Org3MSPanchors.tx
install:
	docker exec cli peer lifecycle chaincode package assignment.tar.gz --path ./chaincode --lang golang --label assignment
	docker exec $(ORG1) cli peer lifecycle chaincode install assignment.tar.gz
	docker exec $(ORG2) cli peer lifecycle chaincode install assignment.tar.gz
	docker exec $(ORG3) cli peer lifecycle chaincode install assignment.tar.gz
	docker exec $(ORG1) cli peer lifecycle chaincode queryinstalled \
		| sed -n "/assignment/{s/^Package ID: //; s/, Label:.*$$//; p;}" > ccid
	docker exec $(ORG1) cli peer lifecycle chaincode approveformyorg $(ORDERER1) --channelID mychannel --name assignment \
		--version 1 --init-required --package-id `cat ccid` --sequence 1 \
		--signature-policy "OutOf(2, 'Org1MSP.peer', 'Org2MSP.peer', 'Org3MSP.peer')"
	docker exec $(ORG2) cli peer lifecycle chaincode approveformyorg $(ORDERER1) --channelID mychannel --name assignment \
		--version 1 --init-required --package-id `cat ccid` --sequence 1 \
		--signature-policy "OutOf(2, 'Org1MSP.peer', 'Org2MSP.peer', 'Org3MSP.peer')"
	docker exec $(ORG3) cli peer lifecycle chaincode approveformyorg $(ORDERER1) --channelID mychannel --name assignment \
		--version 1 --init-required --package-id `cat ccid` --sequence 1 \
		--signature-policy "OutOf(2, 'Org1MSP.peer', 'Org2MSP.peer', 'Org3MSP.peer')"
	docker exec $(ORG1) cli peer lifecycle chaincode commit $(ORDERER1) --channelID mychannel --name assignment $(TWOPEERS) \
		--version 1 --sequence 1 --init-required --signature-policy "OutOf(2, 'Org1MSP.peer', 'Org2MSP.peer', 'Org3MSP.peer')"
	docker exec $(ORG1) cli peer lifecycle chaincode querycommitted --channelID mychannel --name assignment
	docker exec $(ORG1) cli peer chaincode invoke $(ORDERER1) --channelID mychannel --name assignment $(TWOPEERS) \
		--isInit -c "{\"Args\":[\"`cat owner.pub`\"]}" --waitForEvent
test:
	docker exec $(ORG1) $(OWNER) cli peer chaincode invoke $(ORDERER1) --channelID mychannel --name assignment $(TWOPEERS) \
		-c "{\"function\": \"createMoney\", \"Args\":[\"1000\"]}" --waitForEvent
	docker exec $(ORG1) $(OWNER) cli peer chaincode invoke $(ORDERER1) --channelID mychannel --name assignment $(TWOPEERS) \
		-c "{\"function\": \"addUser\", \"Args\":[\"`cat user.pub`\"]}" --waitForEvent
	docker exec $(ORG1) $(OWNER) cli peer chaincode invoke $(ORDERER1) --channelID mychannel --name assignment $(TWOPEERS) \
		-c "{\"function\": \"transfer\", \"Args\":[\"`cat user.pub`\", \"100\"]}" --waitForEvent
	docker exec $(ORG2) $(USER) cli peer chaincode invoke $(ORDERER1) --channelID mychannel --name assignment $(TWOPEERS) \
		-c "{\"function\": \"transfer\", \"Args\":[\"`cat owner.pub`\", \"50\"]}" --waitForEvent
clean:
	rm -rf crypto-config artifacts assignment.tar.gz owner.pub user.pub ccid
