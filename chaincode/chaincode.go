package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/hyperledger/fabric-chaincode-go/pkg/cid"
	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-protos-go/peer"
	"strconv"
)

type chaincode struct {}

func (c *chaincode) Init(stub shim.ChaincodeStubInterface) peer.Response {
	args := stub.GetStringArgs()
	if len(args) != 1 {
		return shim.Error("incorrect argument count")
	}
	if _, err := hex.DecodeString(args[0]); err != nil {
		return shim.Error(fmt.Sprintf("incorrect public key: %s", err.Error()))
	}
	if err := stub.PutState("owner", []byte(args[0])); err != nil {
		return shim.Error(fmt.Sprintf("couldn't save owner's key: %s", err.Error()))
	}
	if err := balanceAdd(stub, args[0], 0, true); err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func (c *chaincode) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	cert, err := cid.GetX509Certificate(stub)
	if err != nil {
		return shim.Error(fmt.Sprintf("couldn't fetch sender's cert: %s", err.Error()))
	}
	pk, ok := cert.PublicKey.(*ecdsa.PublicKey)
	if !ok {
		return shim.Error("public key isn't a ecdsa public key")
	}
	sender := hex.EncodeToString(elliptic.Marshal(pk.Curve, pk.X, pk.Y))
	fn, args := stub.GetFunctionAndParameters()
	switch fn {
	case "createMoney":
		return createMoney(stub, sender, args)
	case "addUser":
		return addUser(stub, args)
	case "transfer":
		return transfer(stub, sender, args)
	default:
		return shim.Error("unknown method")
	}
}

func createMoney(stub shim.ChaincodeStubInterface, sender string, args []string) peer.Response {
	owner, err := stub.GetState("owner")
	if err != nil {
		return shim.Error(fmt.Sprintf("couldn't fetch owner's key: %s", err.Error()))
	}
	if sender != string(owner) {
		return shim.Error("this method might be called only by owner")
	}
	if len(args) != 1 {
		return shim.Error("incorrect argument count")
	}
	amount, err := strconv.ParseUint(args[0], 10, 64)
	if err != nil {
		return shim.Error(fmt.Sprintf("incorrect amount: %s", err.Error()))
	}
	if err := balanceAdd(stub, sender, amount, false); err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func addUser(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 1 {
		return shim.Error("incorrect argument count")
	}
	if _, err := hex.DecodeString(args[0]); err != nil {
		return shim.Error(fmt.Sprintf("incorrect public key: %s", err.Error()))
	}
	if err := balanceAdd(stub, args[0], 0, true); err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func transfer(stub shim.ChaincodeStubInterface, sender string, args []string) peer.Response {
	if len(args) != 2 {
		return shim.Error("incorrect argument count")
	}
	if _, err := hex.DecodeString(args[0]); err != nil {
		return shim.Error(fmt.Sprintf("incorrect public key: %s", err.Error()))
	}
	amount, err := strconv.ParseUint(args[1], 10, 64)
	if err != nil {
		return shim.Error(fmt.Sprintf("incorrect amount: %s", err.Error()))
	}
	if err := balanceSub(stub, sender, amount); err != nil {
		return shim.Error(err.Error())
	}
	if err := balanceAdd(stub, args[0], amount, false); err != nil {
		return shim.Error(err.Error())
	}
	return shim.Success(nil)
}

func balanceLoad(stub shim.ChaincodeStubInterface, pk string, createUnlessExists bool) (string, uint64, error) {
	key, err := stub.CreateCompositeKey("balance", []string{pk})
	if err != nil {
		return "", 0, errors.New(fmt.Sprintf("couldn't create user's key: %s", err.Error()))
	}
	data, err := stub.GetState(key)
	if err != nil {
		return "", 0, errors.New(fmt.Sprintf("couldn't fetch user's balance: %s", err.Error()))
	}
	if len(data) == 0 && createUnlessExists {
		return key, 0, nil
	}
	if len(data) == 0 {
		return "", 0, errors.New("unknown user")
	}
	balance, err := strconv.ParseUint(string(data), 10, 64)
	if err != nil {
		return "", 0, errors.New(fmt.Sprintf("couldn't fetch user's balance: %s", err.Error()))
	}
	return key, balance, nil
}

func balanceAdd(stub shim.ChaincodeStubInterface, pk string, amount uint64, createUnlessExists bool) error {
	key, balance, err := balanceLoad(stub, pk, createUnlessExists)
	if err != nil {
		return err
	}
	if balance + amount < balance {
		return errors.New("balance overflow")
	}
	if err := stub.PutState(key, []byte(strconv.FormatUint(balance + amount, 10))); err != nil {
		return errors.New(fmt.Sprintf("couldn't save user's balance: %s", err.Error()))
	}
	return nil
}

func balanceSub(stub shim.ChaincodeStubInterface, pk string, amount uint64) error {
	key, balance, err := balanceLoad(stub, pk, false)
	if err != nil {
		return err
	}
	if balance < amount {
		return errors.New("insufficient funds")
	}
	if err := stub.PutState(key, []byte(strconv.FormatUint(balance - amount, 10))); err != nil {
		return errors.New(fmt.Sprintf("couldn't save user's balance: %s", err.Error()))
	}
	return nil
}

func main() {
	if err := shim.Start(new(chaincode)); err != nil {
		fmt.Printf("Error starting chaincode: %s", err)
	}
}

