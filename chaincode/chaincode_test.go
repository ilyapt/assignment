package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"github.com/gogo/protobuf/proto"
	"github.com/google/uuid"
	"github.com/hyperledger/fabric-chaincode-go/shimtest"
	"github.com/hyperledger/fabric-protos-go/msp"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"
)

const ownerCert = "../crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/msp/signcerts/User1@org1.example.com-cert.pem"
const userCert = "../crypto-config/peerOrganizations/org2.example.com/users/User1@org2.example.com/msp/signcerts/User1@org2.example.com-cert.pem"

func Test(t *testing.T) {
	ownerPK, owner := parseCert(t, ownerCert)
	userPK, user := parseCert(t, userCert)

	stub := shimtest.NewMockStub("test", new(chaincode))
	resp := stub.MockInit(uuid.New().String(), [][]byte{[]byte(ownerPK)})
	assert.Equal(t, int32(200), resp.Status, resp.Message)

	// negative
	t.Run("create money by user (negative)", func(t *testing.T) {
		stub.Creator = user
		resp = invoke(stub, "createMoney", "1000")
		assert.Equal(t, int32(500), resp.Status)
		assert.Equal(t, "this method might be called only by owner", resp.Message)
	})

	// positive
	t.Run("create money by owner", func(t *testing.T) {
		stub.Creator = owner
		resp = invoke(stub, "createMoney", "1000")
		assert.Equal(t, int32(200), resp.Status, resp.Message)
	})

	// negative
	t.Run("transfer money to nonexistent user (negative)", func(t *testing.T) {
		stub.Creator = owner
		resp = invoke(stub, "transfer", userPK, "100")
		assert.Equal(t, int32(500), resp.Status)
		assert.Equal(t, "unknown user", resp.Message)
	})

	// positive
	t.Run("add user", func(t *testing.T) {
		stub.Creator = owner
		resp = invoke(stub, "addUser", userPK)
		assert.Equal(t, int32(200), resp.Status, resp.Message)
	})

	// negative
	t.Run("transfer money with empty balance (negative)", func(t *testing.T) {
		stub.Creator = user
		resp = invoke(stub, "transfer", ownerPK, "100")
		assert.Equal(t, int32(500), resp.Status)
		assert.Equal(t, "insufficient funds", resp.Message)
	})

	// positive
	t.Run("transfer money to user", func(t *testing.T) {
		stub.Creator = owner
		resp = invoke(stub, "transfer", userPK, "100")
		assert.Equal(t, int32(200), resp.Status, resp.Message)
	})

	t.Run("final balance check", func(t *testing.T) {
		userKey, err := stub.CreateCompositeKey("balance", []string{string(userPK)})
		assert.NoError(t, err)
		userBalance, err := stub.GetState(userKey)
		assert.NoError(t, err)
		assert.Equal(t, "100", string(userBalance))
		ownerKey, err := stub.CreateCompositeKey("balance", []string{string(ownerPK)})
		assert.NoError(t, err)
		ownerBalance, err := stub.GetState(ownerKey)
		assert.NoError(t, err)
		assert.Equal(t, "800", string(ownerBalance))
		// 800 because negative case in mock stub environment also reduces owner balance
	})
}

func invoke(stub *shimtest.MockStub, fn string, args ...string) peer.Response {
	kArgs := make([][]byte, 0, len(args)+1)
	kArgs = append(kArgs, []byte(fn))
	for _, arg := range args {
		kArgs = append(kArgs, []byte(arg))
	}
	return stub.MockInvoke(uuid.New().String(), kArgs)
}

func parseCert(t *testing.T, file string) (string, []byte) {
	certBytes, err := ioutil.ReadFile(file)
	assert.NoError(t, err)

	sender, err := proto.Marshal(&msp.SerializedIdentity{
		Mspid:   "TestOrg",
		IdBytes: certBytes,
	})
	assert.NoError(t, err)

	block, _ := pem.Decode(certBytes)
	assert.Greater(t, len(block.Bytes), 0)
	cert, err := x509.ParseCertificate(block.Bytes)
	assert.NoError(t, err)
	pk, ok := cert.PublicKey.(*ecdsa.PublicKey)
	assert.True(t, ok)
	return hex.EncodeToString(elliptic.Marshal(pk.Curve, pk.X, pk.Y)), sender
}

